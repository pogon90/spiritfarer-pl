key: boatstructuredecorations.decoration_boat_figurehead1._descriptionloc 6787980205210902705 v1 Revised
A figurehead of snakes intertwined. Makes the boat go ((20% Faster)).

key: boatstructuredecorations.decoration_boat_figurehead1._nameloc 6724739233510368489 v1 Revised
Hermes' Hymn

key: boatstructuredecorations.decoration_boat_figurehead2._descriptionloc -6154246785714942220 v1 Revised
A figurehead of beautiful flowers. Makes the boat go ((40% Faster)).

key: boatstructuredecorations.decoration_boat_figurehead2._nameloc -75596110244722814 v1 Revised
Choral Bouquet

key: boatstructuredecorations.decoration_boat_figurehead3._descriptionloc -6140363993271520849 v1 Revised
A figurehead of the nighttime. Makes the boat go ((60% Faster)).

key: boatstructuredecorations.decoration_boat_figurehead3._nameloc 5821986088104538589 v1 Revised
Moon Solo

key: boatstructuredecorations.decoration_boat_figurehead4._descriptionloc -5790294560559887277 v1 Revised
A figurehead praising the Sun. Makes the boat go ((80% Faster)).

key: boatstructuredecorations.decoration_boat_figurehead4._nameloc 1865314388472673491 v1 Revised
Sun Ballad

key: boatstructuredecorations.decoration_boat_temp_figurehead1._nameloc 8198655340101008458 v1 Revised
Moon Solo

key: boatstructuredecorations.decoration_building_basilisk_figurines._descriptionloc 1179178759472602423 v1 Revised
From Buck's favourite shows and games.

key: boatstructuredecorations.decoration_building_basilisk_figurines._nameloc -7016715345583277954 v1 Revised
Various Figurines

key: boatstructuredecorations.decoration_building_basilisk_posters._descriptionloc -4028422490291905635 v1 Revised
Movies and personal art.

key: boatstructuredecorations.decoration_building_basilisk_posters._nameloc -487003027830822314 v1 Revised
Posters

key: boatstructuredecorations.decoration_building_basilisk_weapons._descriptionloc 8868169693685591141 v1 Revised
Cuts your enemies down.

key: boatstructuredecorations.decoration_building_basilisk_weapons._nameloc 3733498684897875319 v1 Revised
Powerful Weapons

key: boatstructuredecorations.decoration_building_bull_decoration1._descriptionloc -7115828194160029805 v1 Revised
Something flashy.

key: boatstructuredecorations.decoration_building_bull_decoration1._hiddendescriptionloc 6815492477160440118 v1 Revised
Looks shiny.

key: boatstructuredecorations.decoration_building_bull_decoration1._hiddennameloc -6473741468844301188 v1 Revised
???

key: boatstructuredecorations.decoration_building_bull_decoration1._nameloc -3984360651965117308 v1 Revised
Chandelier

key: boatstructuredecorations.decoration_building_bull_decoration2._descriptionloc 8967943900503808109 v1 Revised
Something that hits hard

key: boatstructuredecorations.decoration_building_bull_decoration2._hiddendescriptionloc 291084855077568948 v1 Revised
Looks expensive for no reason.

key: boatstructuredecorations.decoration_building_bull_decoration2._hiddennameloc -1396357295662203974 v1 Revised
???

key: boatstructuredecorations.decoration_building_bull_decoration2._nameloc 6843446914926937469 v1 Revised
Liquor Table

key: boatstructuredecorations.decoration_building_bull_decoration3._descriptionloc -38042375968437437 v1 Revised
Something big.

key: boatstructuredecorations.decoration_building_bull_decoration3._hiddendescriptionloc 2430957414241120328 v1 Revised
Bigger isn't better.

key: boatstructuredecorations.decoration_building_bull_decoration3._hiddennameloc -1573652967198774395 v1 Revised
???

key: boatstructuredecorations.decoration_building_bull_decoration3._nameloc 3947177205016211788 v1 Revised
Home Cinema

key: boatstructuredecorations.decoration_building_deer_bookcase._descriptionloc -3556451571669153349 v1 Revised
A collection of Gwen's favourite books.

key: boatstructuredecorations.decoration_building_deer_bookcase._nameloc 6552890137042216555 v1 Revised
Bookcase

key: boatstructuredecorations.decoration_building_deer_reading_corner._descriptionloc -303693273088660215 v1 Revised
A place to rest after a long day.

key: boatstructuredecorations.decoration_building_deer_reading_corner._nameloc -5915323938811628998 v1 Revised
Reading Corner

key: boatstructuredecorations.decoration_building_deer_wall_deco._descriptionloc 811136483967543707 v1 Revised
Frames to spruce up the home.

key: boatstructuredecorations.decoration_building_deer_wall_deco._nameloc 5879862751713853040 v1 Revised
Wall Decorations

key: boatstructuredecorations.decoration_building_dog_frames._descriptionloc -1837150611126644482 v1 Revised
Scenes from nature.

key: boatstructuredecorations.decoration_building_dog_frames._nameloc -3906153571383205084 v1 Revised
Picture Frames

key: boatstructuredecorations.decoration_building_dog_lamp._descriptionloc -6898427788786798159 v1 Revised
To help diffuse the light.

key: boatstructuredecorations.decoration_building_dog_lamp._nameloc -5019026337889633644 v1 Revised
Abat-Jour

key: boatstructuredecorations.decoration_building_dog_plant._descriptionloc 7636901077001864351 v1 Revised
Some greenery.

key: boatstructuredecorations.decoration_building_dog_plant._nameloc -5847704511278620917 v1 Revised
Plant

key: boatstructuredecorations.decoration_building_frog_desk._descriptionloc -8287319677606789187 v1 Revised
A place for Atul to get some woodworking done.

key: boatstructuredecorations.decoration_building_frog_desk._nameloc -5372865763427292703 v1 Revised
Desk

key: boatstructuredecorations.decoration_building_frog_picture_frame._descriptionloc -3047049746768915196 v1 Revised
A picture from Atul's old college days.

key: boatstructuredecorations.decoration_building_frog_picture_frame._nameloc -4688822106075665650 v1 Revised
Picture Frame

key: boatstructuredecorations.decoration_building_frog_shower._descriptionloc 2273783919404758322 v1 Revised
To wash off all the sawdust.

key: boatstructuredecorations.decoration_building_frog_shower._nameloc -632079138798152351 v1 Revised
Shower

key: boatstructuredecorations.decoration_building_hedgehog_frames._descriptionloc 5019369976793915899 v1 Revised
Family photographs

key: boatstructuredecorations.decoration_building_hedgehog_frames._nameloc 7736186516475464875 v1 Revised
Framed Photographs

key: boatstructuredecorations.decoration_building_hedgehog_shelf._descriptionloc 7366902474429673127 v1 Revised
A valuable collection of dishes handed down from generation to generation.

key: boatstructuredecorations.decoration_building_hedgehog_shelf._nameloc -1227783562509191070 v1 Revised
Commemorative Dish Collection

key: boatstructuredecorations.decoration_building_hedgehog_wardrobe._descriptionloc -4176088762804055641 v1 Revised
Change into a fresh set of clothes.

key: boatstructuredecorations.decoration_building_hedgehog_wardrobe._nameloc 2627152636766791011 v1 Revised
Wardrobe

key: boatstructuredecorations.decoration_building_lynx_fridge._descriptionloc 4085800458057996420 v1 Revised
Keeps your drinks at the right temperature.

key: boatstructuredecorations.decoration_building_lynx_fridge._nameloc 575936999349250033 v1 Revised
Small Fridge

key: boatstructuredecorations.decoration_building_lynx_table._descriptionloc 4054738324961646172 v1 Revised
Nothing says "Welcome Home" more than a faded plant.

key: boatstructuredecorations.decoration_building_lynx_table._nameloc -2878997191456885156 v1 Revised
Old Plant

key: boatstructuredecorations.decoration_building_lynx_tv._descriptionloc -7174309530188230700 v1 Revised
For when you want to watch your stories.

key: boatstructuredecorations.decoration_building_lynx_tv._nameloc -1363161684225175453 v1 Revised
TV Set

key: boatstructuredecorations.decoration_building_owl_clock._descriptionloc 2317240183297923399 v1 Revised
Combines both the perks of being unreadable and taking up half the room!

key: boatstructuredecorations.decoration_building_owl_clock._nameloc 8371069464291905360 v1 Revised
Maximinimalist Clock

key: boatstructuredecorations.decoration_building_owl_desk._descriptionloc -1439447672506954876 v1 Revised
This desk's form and function are elegantly working toward the same goal: showing off its owners impeccable taste.

key: boatstructuredecorations.decoration_building_owl_desk._nameloc 8818175407221148218 v1 Revised
Vintage Modernist Desk

key: boatstructuredecorations.decoration_building_owl_fireplace._descriptionloc 7173274711101944073 v1 Revised
Designed to keep the room as cold as possible.

key: boatstructuredecorations.decoration_building_owl_fireplace._nameloc 5197553190972105266 v1 Revised
Neoclassical Nordic Fireplace

key: boatstructuredecorations.decoration_building_snake_crystal_table._descriptionloc -5735498882719951399 v1 Revised
The best table to converge all your energy.

key: boatstructuredecorations.decoration_building_snake_crystal_table._nameloc 6840927008690144678 v1 Revised
Crystal Table

key: boatstructuredecorations.decoration_building_snake_heater._descriptionloc -844855925279985829 v1 Revised
A complete, all-in-one, professional quality, heating, ventilation and air conditioning unit.

key: boatstructuredecorations.decoration_building_snake_heater._nameloc -9176551842073655654 v1 Revised
HVAC System

key: boatstructuredecorations.decoration_building_snake_monstera._descriptionloc 939900421721509538 v1 Revised
Also known as the Cheese Plant. It is neither cheese nor is it delicious. Trust me.

key: boatstructuredecorations.decoration_building_snake_monstera._nameloc -3942986175243012617 v1 Revised
Monstera Deliciosa

key: boatstructuredecorations.decoration_building_stella_upgradetemp_rockbreaker._descriptionloc -120519198950082269 v1 Placeholder
Gotta go fast

key: boatstructuredecorations.decoration_building_stella_upgradetemp_rockbreaker._nameloc -2899782344854987075 v1 Placeholder
Fast Travel

key: boatstructuredecorations.decoration_building_temphouse_hanging_lights._descriptionloc -1416895808588625541 v1 Revised
Add lights for your passengers.

key: boatstructuredecorations.decoration_building_temphouse_hanging_lights._nameloc 9030895615946585316 v1 Revised
Hanging Lights

key: boatstructuredecorations.decoration_station_cereal_field_lantern._descriptionloc -2001502212178349118 v1 Revised
More light for the outside.

key: boatstructuredecorations.decoration_station_cereal_field_lantern._nameloc 8709070548507849757 v1 Revised
Lantern

key: boatstructuredecorations.decoration_station_coop_upgrade._descriptionloc 2081168011624496553 v1 Revised
Help your chickens help themselves with more food.

key: boatstructuredecorations.decoration_station_coop_upgrade._nameloc -5284162686035719520 v1 Revised
Chicken Coop Upgrade

key: boatstructuredecorations.decoration_station_cow_upgrade._descriptionloc 4709419211764788311 v1 Revised
Your cows will be less hungry all the time!

key: boatstructuredecorations.decoration_station_cow_upgrade._nameloc -2055915813657501752 v1 Revised
Cow Stall Upgrade

key: boatstructuredecorations.decoration_station_crusher_upgrade._descriptionloc 2337738946005834800 v1 Revised
Now yield more resources because of all that crushing power.

key: boatstructuredecorations.decoration_station_crusher_upgrade._nameloc 8335001377205268983 v1 Revised
Crusher Upgrade

key: boatstructuredecorations.decoration_station_field_upgrade._descriptionloc 155748520974845906 v1 Revised
See your cereal grow faster.

key: boatstructuredecorations.decoration_station_field_upgrade._nameloc 2404982117756691937 v1 Revised
Field Upgrade

key: boatstructuredecorations.decoration_station_field_water_upgrade._descriptionloc 6699642694948935102 v0 ReadyForRevision


key: boatstructuredecorations.decoration_station_field_water_upgrade._nameloc -5382764023182381613 v0 ReadyForRevision


key: boatstructuredecorations.decoration_station_forge_upgrade._descriptionloc 3442943541027443934 v1 Revised
Just like that, you can become a master blacksmith.

key: boatstructuredecorations.decoration_station_forge_upgrade._nameloc -651379785442576271 v1 Revised
Smithy Upgrade

key: boatstructuredecorations.decoration_station_garden_upgrade._descriptionloc -7944445446224846130 v1 Revised
Let those vegetables breathe and swing!

key: boatstructuredecorations.decoration_station_garden_upgrade._nameloc -4416615242348476901 v1 Revised
Garden Upgrade

key: boatstructuredecorations.decoration_station_garden_water_upgrade._descriptionloc -2005970830265119844 v0 ReadyForRevision


key: boatstructuredecorations.decoration_station_garden_water_upgrade._nameloc -5963299563144995281 v0 ReadyForRevision


key: boatstructuredecorations.decoration_station_kitchen_small_timer._descriptionloc -3353327713239262163 v1 Revised
Say goodbye to burnt food!

key: boatstructuredecorations.decoration_station_kitchen_small_timer._nameloc 5027409403212745994 v1 Revised
Timer

key: boatstructuredecorations.decoration_station_kitchen_stove_upgrade._descriptionloc 2598792775951465723 v1 Revised
Unlock the full potential of yum!

key: boatstructuredecorations.decoration_station_kitchen_stove_upgrade._nameloc 10455851016628770 v1 Revised
Kitchen Upgrade

key: boatstructuredecorations.decoration_station_loom_lamp._descriptionloc -717075400472647219 v1 Revised
Really brings the whole room together.

key: boatstructuredecorations.decoration_station_loom_lamp._nameloc -810452852666533167 v1 Revised
Lamp

key: boatstructuredecorations.decoration_station_loom_upgrade._descriptionloc 434412035549472220 v1 Revised
Weave swiftly like all your tailoring idols.

key: boatstructuredecorations.decoration_station_loom_upgrade._nameloc -7328483949356379115 v1 Revised
Loom Upgrade

key: boatstructuredecorations.decoration_station_mill_upgrade._descriptionloc 6837595358552003016 v1 Revised
Really teach that grain who's boss.

key: boatstructuredecorations.decoration_station_mill_upgrade._nameloc -1784492434623980718 v1 Revised
Mill Upgrade

key: boatstructuredecorations.decoration_station_orchard_upgrade._descriptionloc -2639238925527678023 v1 Revised
You'll be picking fruit all day now!

key: boatstructuredecorations.decoration_station_orchard_upgrade._nameloc -188859740818979238 v1 Revised
Orchard Upgrade

key: boatstructuredecorations.decoration_station_sawmill_upgrade._descriptionloc 5953607661390057469 v1 Revised
You'll get all the logs you've been dreaming of.

key: boatstructuredecorations.decoration_station_sawmill_upgrade._nameloc 7411495099421547108 v1 Revised
Sawmill Upgrade

key: boatstructuredecorations.decoration_station_sheep_enclosure_upgrade._descriptionloc -1527850005508285620 v1 Revised
Hungry sheep are a thing of the past!

key: boatstructuredecorations.decoration_station_sheep_enclosure_upgrade._nameloc -8894308783130604237 v1 Revised
Sheep Corral Upgrade

key: boatstructuredecorations.decoration_station_shelf_upgrade._descriptionloc 4290264322790182716 v1 Revised
The power of extra fermentation is at your fingertips.

key: boatstructuredecorations.decoration_station_shelf_upgrade._nameloc -1698196376894153091 v1 Revised
Cellar Upgrade

key: boatstructuredecorations.decoration_station_smelter_upgrade._descriptionloc -52094897400657430 v1 Revised
See your profits double with all those ingots.

key: boatstructuredecorations.decoration_station_smelter_upgrade._nameloc -3380219254585797580 v1 Revised
Foundry Upgrade

key: boatstructuredecorations.decoration_station_veggie_garden_lantern._descriptionloc 1990353233540463732 v1 Revised
A light outside your garden.

key: boatstructuredecorations.decoration_station_veggie_garden_lantern._nameloc -5427668375132329515 v1 Revised
Lantern

