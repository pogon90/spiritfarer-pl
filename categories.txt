key: categories.blueprintcategory.displaynameloc 1978366105650178771 v1 Revised
Blueprints

key: categories.clothcategory.displaynameloc 3132252953898431982 v1 Revised
Cloths

key: categories.defaultcategory.displaynameloc 518809841925276903 v1 Revised
Unsorted

key: categories.dishescollection._displayname 2308126902668756248 v1 Revised
Simple Dishes

key: categories.documentcategory.displaynameloc -6044690524689812141 v1 Revised
Documents

key: categories.elaboratedishescategory.displaynameloc 7322361375158485858 v1 Revised
Elaborate Dishes

key: categories.figurinescategory.displaynameloc 8515724629656846851 v1 Revised
Figurines

key: categories.fishcategory.displaynameloc -2905540938941058612 v1 Revised
Fishing

key: categories.fruitsandveggiecategory.displaynameloc -4474090542304593826 v1 Revised
Fruits & Veggies

key: categories.gemcategory.displaynameloc 6817210525401773451 v1 Revised
Gems

key: categories.glasscategory.displaynameloc 347658884133089344 v1 Revised
Glass

key: categories.graincategory.displaynameloc -5424855022587808759 v1 Revised
Grains

key: categories.ingredientscategory.displaynameloc 9048149078627796843 v1 Revised
Ingredients

key: categories.metalcategory.displaynameloc 2481771863220760850 v1 Revised
Metals

key: categories.other.displaynameloc -6274462692442991073 v1 Revised
Tchotchke

key: categories.questcategory.displaynameloc 7537937604056209906 v1 Revised
Special

key: categories.resourcestations.displaynameloc 4812140498525567078 v1 Revised
Resource Stations

key: categories.rockcategory.displaynameloc -167146888657647639 v1 Revised
Rocks

key: categories.seedscategory.displaynameloc 4041483453894176787 v1 Revised
Seeds

key: categories.simpledishescategory.displaynameloc -1378286114762069121 v1 Revised
Dishes

key: categories.spirithomes.displaynameloc 4525925752016087475 v1 Revised
Spirit Homes

key: categories.woodcategory.displaynameloc 75181213344993340 v1 Revised
Wood

