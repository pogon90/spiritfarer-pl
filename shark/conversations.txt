key: shark/conversations._snake-buildheatersharkconversation._independenttextbubbleloc 8379812204377198575 v1 Revised
What is it?

key: shark/conversations._snake-buildheatersharkconversation.loctext 5739457988909103323 v1 Revised
{|Expression_Surprise}Oh, if I don't know that face!
{|Expression_Surprise}It's about that house, the green and brown one, right?
Have some sort of problem with it, don't you?
Yeah, I could see it from a mile away.
{|Expression_Pensive}Listen, I know my job. Me and the boys, we're real professionals.
{|Expression_Pensive}And it's not often that we criticise another worker's craftsmanship
{|Expression_Pensive}But that house, oh wow.
{|Expression_Pensive}It had to be worst job I've ever seen.
[laugh]{|Expression_Unhappy}Rarara!
So consider this a lesson. Don't just trust any old amateur.
Always go to a professional!
So, tell me, what complaints does your passenger have?
What, she's sleeping?
Oh, of course.
{|Expression_Pensive}That bald head of hers is very sensitive to temperature changes.
{|Expression_Pensive}Imagine how cold she must be.
Not mentioning her dry and scaly skin.
No, no, she needs a temperature and humidity control system.
{|Expression_Surprise}Luckily, that place was so shoddily designed that there's ample cracks for wiring and electronics.
[laugh]{|Expression_Unhappy}Rarara!
So, yeah, there you go. Some plans and schematics for a nice, all-in-one HVAC system.
Pro quality!
Call it a courtesy. After all, how can you repay your debt if your boat falls to pieces?
[laugh]{|Expression_Unhappy}Rarara!

key: shark/conversations.sharkcall.loctext -7985986817627023309 v1 Revised
Over here!

key: shark/conversations.sharkconversation-buy01.loctext -6645534362166049406 v1 Revised
Welcome to my shipyard, young traveller.
Where my prices are a customer’s best friend.
So, what do you need?

key: shark/conversations.sharkconversation-buy02.loctext 2525090003300057175 v1 Revised
Welcome back to the shipyard, Stella!
Tell me what you need?

key: shark/conversations.sharkconversation-intro._independenttextbubbleloc 6873576788641554616 v1 Revised
Over here!

key: shark/conversations.sharkconversation-intro.loctext -2800986088989375864 v1 Revised
{|Expression_Surprise}Welcome to my shipyard, young traveller!
{|Expression_Idle_Neutral}[Laugh]Where my prices are a customer’s best friend!
{|Expression_Idle_Neutral}I'm Albert!
{|Expression_Idle_Neutral}Been in the business of shipbuilding for ages.
{|Expression_Idle_Neutral}Should I assume you are the new Spiritfarer?
{|Expression_Headnod}Well, of course, you are!
Anyway, you've been able to start this ship and bring it here after all!
Gwen was right to point you towards my humble enterprise.
Ha, let me see what you have here...
[Laugh]Nice little dinghy!
{|Expression_Idle_Neutral}Funny how it looks exactly like the one I'd built some time ago and stored in my old shipyard!
{|Expression_Idle_Neutral}See, Charon was unhappy about his gloomy and dank Ark.
{|Expression_Idle_Neutral}Actually, he was always complaining about everything.
{|Expression_Idle_Neutral}But the customer's always right!
{|Expression_Idle_Neutral}So I built him a newer, better one.
{|Expression_Idle_Neutral}Of course, he would never abandon his previous boat.
{|Expression_Idle_Neutral}Old habits die hard!
{|Expression_Idle_Neutral}But don't worry, you can keep this one!
{|Expression_Idle_Neutral}[Laugh]Let's just call this a loan.
{|Expression_Idle_Neutral}And let me tell you something.
{|Expression_Idle_Neutral}You know we always joke about the construction here?
{|Expression_Idle_Neutral}Don't worry.
{|Expression_Unhappy}We're still working on it.
{|Expression_Unhappy}[Laugh]Rararara!
{|Expression_Headnod}Anyway, with a little bit of love, this bird will be resplendent again!
{|Expression_Headnod}The first thing you'll need is a proper ((Blueprint Table)).
Since it's your lucky day, and I'm feeling generous, I'll offer it to you!
Free of charge!
{|Expression_Idle_Neutral}Mind you, it's just a bit barebones in terms of functionality.
{|Expression_Unhappy}[Laugh]But the subsequent upgrades will come at such a small price, you'll want to buy them almost immediately!
Simply direct me to ((Upgrade)) the ship!

key: shark/conversations.sharkconversation-joke01.loctext 2664222340528709697 v1 Revised
{|Expression_Idle_Neutral}I once had a friend who cut his own toe.
{|Expression_Idle_Neutral}He had a new one made of rubber.
{|Expression_Unhappy}He was called Roberto.
{|Expression_Unhappy}Get it?
[Laugh]{|Expression_Unhappy}Rarrararrrrr!!!

key: shark/conversations.sharkconversation-joke03.loctext 2430783117394164875 v1 Revised
Did you know that I'm the best at sleeping?
{|Expression_Unhappy}So good in fact that that I can do it with my eyes closed.
[Laugh]{|Expression_Unhappy}Rarrarararrrrr!!!

key: shark/conversations.sharkconversation-joke04.loctext -7122724653160910601 v1 Revised
We are closing early today, Stella.
{|Expression_Idle_Neutral}I have an appointment at the dentist.
{|Expression_Unhappy}It's at Tooth 'O Clock.
[Laugh]Rarrarararrrrr!!!

key: shark/conversations.sharkconversation-joke-cellar.loctext -3233154901028731055 v1 Revised
{|Expression_Idle_Neutral}I hope there’s a mirror in that ((Cellar)) of yours!
{|Expression_Unhappy}Because it’s looking gouda!
[Laugh]{|Expression_Unhappy}Rararararrr!!!

key: shark/conversations.sharkconversation-joke-coop.loctext -7312466413048285473 v1 Revised
{|Expression_Idle_Neutral}My mother had a ((Coop)) on her childhood farm.
{|Expression_Idle_Neutral}...
{|Expression_Idle_Neutral}Oh, my mother!
{|Expression_Idle_Neutral}She wasn’t mad at me when I did bad things.
{|Expression_Idle_Neutral}She was disappointed in me!
{|Expression_Unhappy}I suppose I wasn’t what I cracked up to be!
[Laugh]{|Expression_Unhappy}Rararararrr!!!

key: shark/conversations.sharkconversation-joke-cow.loctext -281864581427330614 v1 Revised
{|Expression_Idle_Neutral}You’ve got a cow on your boat.
{|Expression_Idle_Neutral}That’s neat.
{|Expression_Idle_Neutral}…
{|Expression_Unhappy}Do you know why cows have hooves instead of feet?
{|Expression_Unhappy}Because they lack toes!
[Laugh]{|Expression_Unhappy}Rararararrr!!!

key: shark/conversations.sharkconversation-joke-crusher.loctext 3966887956651774948 v1 Revised
{|Expression_Idle_Neutral}Are you hungry?
{|Expression_Idle_Neutral}I know a great place to eat.
{|Expression_Idle_Neutral}It’s called The Desert.
{|Expression_Unhappy}It’s famous for its sand which I’m sure you’ll like.
[Laugh]{|Expression_Unhappy}Rararararrr!!!

key: shark/conversations.sharkconversation-joke-field.loctext 1760080745831833150 v1 Revised
{|Expression_Idle_Neutral}I heard they just fired an employee at the popcorn factory.
{|Expression_Idle_Neutral}He was a good worker.
{|Expression_Idle_Neutral}But...
{|Expression_Unhappy}They found him sleeping on the cob!
[Laugh]{|Expression_Unhappy}Rararararrr!!!

key: shark/conversations.sharkconversation-joke-forge.loctext 9170208586495626132 v1 Revised
{|Expression_Idle_Neutral}My friend, John, has been sent to prison.
{|Expression_Idle_Neutral}He was a blacksmith.
{|Expression_Unhappy}…
{|Expression_Unhappy}He was caught forging other people’s signatures.
[Laugh]{|Expression_Unhappy}Rararararrr!!!

key: shark/conversations.sharkconversation-joke-garden.loctext 1875596615725924589 v1 Revised
That’s a nice ((Garden)).
Knock! Knock!
{|Expression_Idle_Neutral}…
{|Expression_Idle_Neutral}Who’s there?
{|Expression_Idle_Neutral}…
{|Expression_Idle_Neutral}Lettuce.
{|Expression_Idle_Neutral}…
{|Expression_Idle_Neutral}Lettuce, who?
{|Expression_Idle_Neutral}...
{|Expression_Unhappy}Lettuce in and you’ll find out!
[Laugh]{|Expression_Unhappy}Rararararrr!!!

key: shark/conversations.sharkconversation-joke-loom.loctext 1042250137991169539 v1 Revised
Wait one second!
I don't have a joke for the ((Loom)) yet.
{|Expression_Idle_Neutral}I might have one soon.
{|Expression_Idle_Neutral}It's coming.
{|Expression_Unhappy}I can feel it...
{|Expression_Unhappy}Looming over me.
[Laugh]{|Expression_Unhappy}Rararararrr!!!

key: shark/conversations.sharkconversation-joke-master._commenttext 2424208133638141113 v1 Revised
Oh! Oh!

key: shark/conversations.sharkconversation-joke-mill.loctext -6428681493167160651 v1 Revised
{|Expression_Idle_Neutral}That ((Mill)) looks impressive.
{|Expression_Unhappy}Did you know what they call one lonely grain of corn in a tree?
{|Expression_Unhappy}…
{|Expression_Unhappy}No, guess!
{|Expression_Unhappy}...
{|Expression_Unhappy}Acorn!
[Laugh]{|Expression_Unhappy}Rararararrr!!!

key: shark/conversations.sharkconversation-joke-orchard.loctext -2299025491933436904 v1 Revised
{|Expression_Idle_Neutral}You've got a beautiful ((Orchard)) full of apples.
{|Expression_Idle_Neutral}You do know what lives and reads inside, don't you?
{|Expression_Idle_Neutral}...
{|Expression_Unhappy}Bookworms.
[Laugh]{|Expression_Unhappy}Rararararrr!!!

key: shark/conversations.sharkconversation-joke-sawmill.loctext 8660813622310031073 v1 Revised
{|Expression_Idle_Neutral}You can do incredible things with a sawmill these days.
{|Expression_Idle_Neutral}You can even go on the internet.
{|Expression_Unhappy}You can LOG in!
{|Expression_Unhappy}Get it?
[Laugh]{|Expression_Unhappy}Rararararrr!!!

key: shark/conversations.sharkconversation-joke-sheep.loctext 4411894692156765303 v1 Revised
{|Expression_Idle_Neutral}Look at your hair!
{|Expression_Idle_Neutral}Looks like someone’s been at the baa-baa shop.
[Laugh]{|Expression_Unhappy}Rararararrr!!!
{|Expression_Unhappy}...
{|Expression_Unhappy}Get it?
{|Expression_Unhappy}Because of the sheep on your boat.
{|Expression_Unhappy}Nevermind.

key: shark/conversations.sharkconversation-joke-smelter.loctext -3610364046362615016 v1 Revised
{|Expression_Idle_Neutral}That’s one mighty foundry!
{|Expression_Idle_Neutral}When they built it...
{|Expression_Unhappy}They were surely thinking outside the bauxite
[Laugh]{|Expression_Unhappy}Rararararrr!!!

key: shark/conversations.sharkconversation-mailboxintro._independenttextbubbleloc 7624926503550797685 v1 Revised
Stella!

key: shark/conversations.sharkconversation-mailboxintro.loctext 2006337971627933080 v1 Revised
I've been getting your mail here for the past month.
{|Expression_Unhappy}I'm not a post office.
{|Expression_Unhappy}You need to add a mailbox to your boat.
{|Expression_Unhappy}If I get one more letter filled with glitter, I'll lose it.
{|Expression_Unhappy}I'm warning you.
[Laugh]{|Expression_Unhappy}You might be a customer, but I reserve the right to yell at you.
{|Expression_Unhappy}Get it done.

key: shark/conversations.sharkconversation-upgradeblueprintstationvalidation._independenttextbubbleloc -4152423872149119206 v1 Revised
There you go.

key: shark/conversations.sharkconversation-upgradeblueprintstationvalidation.loctext 3940598783648264165 v1 Revised
You'll be able to ((Build)) and ((Edit)) your buildings on your boat.
What a treat.
[Laugh]{|Expression_Unhappy}But, if you really want to ((Upgrade)) your boat in size and in pleasure, I'm your guy.
Come back when you feel your boat is getting too crowded.
Cheers.

