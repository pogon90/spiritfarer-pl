key: hints.acquireability_bounce.message -5694324996668490490 v1 Revised
Nacisnij $JUMP$ aby podskoczyc wyzej na elastycznych powierzchniach.

key: hints.acquireability_bounce.title 3172111628004666582 v1 Revised
Bounce

key: hints.acquireability_dash.message -8228152922982453908 v1 Revised
Press $DASH$ to release Everlight energy and leap forward.

key: hints.acquireability_dash.title 1103306112950547402 v1 Revised
Dash

key: hints.acquireability_doublejump.description 2359294598557301363 v1 Placeholder
"The power of jumping really high is quite amusing" -Edward Lodewijk

key: hints.acquireability_doublejump.message -9108576666337613796 v1 Revised
Gdy jestes w powietrzu, nacisnij ponownie $JUMP$ aby wykonac podwojny skok!

key: hints.acquireability_doublejump.title -4509950748026569574 v1 Revised
Podwojny skok

key: hints.acquireability_fishplus.message -8383269769448317351 v1 Placeholder
???? FISH+ UPDATE TEXT NEEDED

key: hints.acquireability_fishplus.title -7726754530197743789 v1 Placeholder
Fish+

key: hints.acquireability_glide.message 7443888113169538592 v1 Revised
While in the air, hold $JUMP$ to glide.

key: hints.acquireability_glide.title 4653079998888790442 v1 Revised
Glide

key: hints.acquireability_lightburst.message -5154723113465695956 v1 Revised
Press $EXPAND_EVERLIGHT$ to release Everlight energy in all directions around you.

key: hints.acquireability_lightburst.title -9142593254082381345 v1 Revised
Light Burst

key: hints.acquireability_mineplus.message -616293915845901656 v1 Revised
An Everlight booster that improves both your mining and your fishing.

key: hints.acquireability_mineplus.title 3096536508095525937 v1 Revised
Improved Tools

key: hints.acquireability_woodplus.message -621384976230861367 v1 Placeholder
???? WOOD+ UPDATE TEXT NEEDED

key: hints.acquireability_woodplus.title -7915030868340095470 v1 Placeholder
Wood+

key: hints.acquireability_zipline.message -1849898702012853504 v1 Revised
Hold $VERTICAL|+$ to zipline on ropes and wires.

key: hints.acquireability_zipline.title -1886207315325968364 v1 Revised
Zipline

key: hints.boat_ring_bell.message 5148932314876176755 v1 Placeholder
Ring the bell to start the boat again.

key: hints.boat_stopped.message -728013281194559415 v1 Revised
When Stella or Daffodil leave the boat, it stops. It will restart automatically once everyone is back on board.

key: hints.chicken_coop_full.message 7962860009579262853 v1 Revised
The chicken coop is full.

key: hints.default.message 7797824792575295793 v1 Placeholder
Default hint

key: hints.diving_treasure.message 7240756406299773267 v1 Revised
Hold $VERTICAL|-$ and press $JUMP$ to dive.

key: hints.fishing_bobber_missed.message -2304196949650982020 v1 Revised
When the bobber is shaking, press $INTERACT$ to start reeling!

key: hints.fishing_hold_input.message 7017965903751825021 v1 Revised
Hold $INTERACT$ to reel.

key: hints.fishing_line_broke.message -7249171646298801347 v1 Revised
When the line turns red, it's about to break. Quickly release $INTERACT$.

key: hints.interact_prompt.message 8761352771455304295 v1 Revised
Press $INTERACT$ to interact with a glowing outline.

key: hints.interact_prompt_to_continue.message -796972931143290581 v1 Revised
Press $INTERACT$

key: hints.jump_tutorial.message -8103842912241083467 v1 Revised
Press $JUMP$ to jump.
Hold $JUMP$ to jump higher.

key: hints.loom_hold_input.message -3641404572990207871 v1 Revised
Hold $INTERACT$ to start weaving.

key: hints.mining_tutorial_long.message 3924139614949642507 v1 Revised
To mine efficiently, release $INTERACT$ as late as possible without failing.

key: hints.mining_tutorial_short.message -8361674015509389452 v1 Revised
Hold $INTERACT$ as long as possible to break a mining node.

key: hints.music_tutorial.message -7352765137924448635 v1 Revised
Hold $CONTEXTUAL_ACTION$ to play the guitar.

key: hints.open_request_log.message -6394495609577920090 v1 Revised
Press $OPEN_REQUEST_LOG$ to review active requests.

key: hints.passthrough_down_tutorial.message -8972414841326485307 v1 Revised
Hold $VERTICAL|-$ and $JUMP$ to drop down.

key: hints.passthrough_tutorial.message -7867668505495339751 v1 Revised
Hold $VERTICAL|+$ while jumping to climb through platforms.

key: hints.plant_dried_out.message -5946422778089002908 v1 Revised
Remember to water your plants!

key: hints.plant_tap_button.message 3010742870746184462 v1 Revised
Hold $INTERACT$ to pull.

key: hints.projector_cabin.message -2031579185289053076 v1 Placeholder
Look inside Stella's cabin to plan your next adventure.

key: hints.projector_cant_start.message -1168055859963670250 v1 Revised
It's too dark to navigate, it will be possible again at dawn.

key: hints.projector_destination.message 20782873179537761 v1 Placeholder
Select a destination you want to visit on the map.

key: hints.projector_projector.message -5096624777463779693 v1 Placeholder
Interact with the projector inside Stella’s cabin.

key: hints.second_player_inactive.message 2488488081875289211 v1 Revised
2-Player Coop is enabled but the second player is inactive. Please disable the second player to continue in single player.

key: hints.showfloor_interact_prompt.message -8226104077722730950 v1 Placeholder
Press $INTERACT$ to interact with objects and passengers.

key: hints.woodcutting_tutorial.message -6039010806837462215 v1 Revised
Move the saw back and forth with $HORIZONTAL|-$ and $HORIZONTAL|+$ to cut a tree.

key: hints.zoom_out_clock.message 8039419584747155431 v1 Revised
Press $SHOW_NAV_INFO$ to toggle the clock and trajectory display on and off

