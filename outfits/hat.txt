key: outfits/hat.dark._name -6044891019996151990 v1 Revised
Dark Hat

key: outfits/hat.default._name -3816694438542443520 v1 Revised
Gold-Hemmed Hat

key: outfits/hat.eshe._name 6034784492653965679 v1 Revised
Ht'aetag'

key: outfits/hat.pastel._name 7523344005447713352 v1 Revised
Sunday Hat

key: outfits/hat.periwinkle._name -2629855139762042985 v1 Revised
Lilac Hat

key: outfits/hat.purple._name 6195342989372963058 v1 Revised
Berry Hat

key: outfits/hat.red._name -6669136093347592794 v1 Revised
Red Hat

key: outfits/hat.specialalicehat._name 3005613033697321234 v1 Revised
Beach Hat

key: outfits/hat.thora._name -2881313147872587988 v1 Revised
Odin's Hat

key: outfits/hat.yellow._name 6897830496598514162 v1 Revised
Turquoise Hat

