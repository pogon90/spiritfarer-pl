key: items/seeds.fieldseed_coffeebeans.description 4257221005888594790 v1 Revised
When planted in the ((Field)), these seeds will grow into ((Coffee Beans)).

key: items/seeds.fieldseed_coffeebeans.displayname 4242051849887017976 v1 Revised
Coffee Beans Seed

key: items/seeds.fieldseed_coffeebeans1.description -3106561594592028976 v1 Revised
When planted in the ((Field)), these seeds will grow into ((Coffee Beans)).

key: items/seeds.fieldseed_coffeebeans1.displayname 4070139696957382101 v1 Revised
Coffee Beans Seed

key: items/seeds.fieldseed_corn.description -2131855160692161444 v1 Revised
When planted in the ((Field)), these seeds will grow into savoury ((Corn)).

key: items/seeds.fieldseed_corn.displayname -2300494736537170254 v1 Revised
Corn Seed

key: items/seeds.fieldseed_cotton.description -2172253797760564747 v1 Revised
When planted in the ((Field)), this seed will grow into cotton. Nothing is stopping you from weaving the results into ((Cotton Thread)).

key: items/seeds.fieldseed_cotton.displayname 1648828165123597772 v1 Revised
Cotton Seed

key: items/seeds.fieldseed_flax.description 5811711189314384597 v1 Revised
When planted in the ((Field)), this will sprout into flax, which in turn can be woven into ((Linen Thread)) with the right tools.

key: items/seeds.fieldseed_flax.displayname 8410161991575313419 v1 Revised
Linen Seed

key: items/seeds.fieldseed_garlic.description 5451119329863535438 v1 Revised
When planted in the ((Field)), this will become a savoury ((Garlic)).

key: items/seeds.fieldseed_garlic.displayname 8112422829234809472 v1 Revised
Garlic Seed

key: items/seeds.fieldseed_rice.description 6741351954353082900 v1 Revised
When planted in the ((Field)), this will sprout into ((Rice)).

key: items/seeds.fieldseed_rice.displayname -6396140885035281886 v1 Revised
Rice Seed

key: items/seeds.fieldseed_sugarcane.description 2665081637339095387 v1 Revised
When planted in the ((Field)), this will sprout into ((Sugar Cane)).

key: items/seeds.fieldseed_sugarcane.displayname -6563787418493600698 v1 Revised
Sugar Cane Seed

key: items/seeds.fieldseed_sunflower.description -2993845815526209893 v1 Revised
When planted in the ((Field)), this will sprout into ((Sunflowers)). These seeds can also be crushed in the ((Crusher)) to give ((Sunflower Oil)).

key: items/seeds.fieldseed_sunflower.displayname -247903741802235396 v1 Revised
Sunflower Seed

key: items/seeds.fieldseed_tealeaves.description 5680389878782688254 v1 Revised
When planted in the ((Field)), these seeds will grow into ((Tea Leaves)).

key: items/seeds.fieldseed_tealeaves.displayname -4734745874609033805 v1 Revised
Tea Seed

key: items/seeds.fieldseed_tomato.description 7993555779027687374 v1 Revised
When planted in the ((Field)), this will sprout into a ((Tomato)).

key: items/seeds.fieldseed_tomato.displayname 8496585246827267272 v1 Revised
Tomato Seed

key: items/seeds.fieldseed_wheat.description -3217746635344196773 v1 Revised
When planted in the ((Field)), this will grow into ((Wheat)).

key: items/seeds.fieldseed_wheat.displayname -8591309283113000997 v1 Revised
Wheat Seed

key: items/seeds.gardenseed_cabbage.description 8202734790562619434 v1 Revised
When planted in the ((Garden)), these seeds will turn into a delectable ((Cabbage)).

key: items/seeds.gardenseed_cabbage.displayname -7346451933337119824 v1 Revised
Cabbage Seed

key: items/seeds.gardenseed_cabbage1.description 3707297745572668819 v1 Revised
When planted in the ((Garden)), these seeds will turn into a delectable ((Cabbage)).

key: items/seeds.gardenseed_cabbage1.displayname -1660844982045150689 v1 Revised
Cabbage Seed

key: items/seeds.gardenseed_carrot.description -1527345638238970992 v1 Revised
When planted in the ((Garden)), these seeds will grow into a tasty ((Carrot)).

key: items/seeds.gardenseed_carrot.displayname 5671413934995133841 v1 Revised
Carrot Seed

key: items/seeds.gardenseed_celery.description -5848128343974697392 v1 Revised
When planted in the ((Garden)), these seeds will turn into a ((Celery)).

key: items/seeds.gardenseed_celery.displayname -3576288680938181726 v1 Revised
Celery Seed

key: items/seeds.gardenseed_fireglow.description -2970547836478852995 v1 Revised
When planted in the ((Garden)), these seeds will turn into a strange ((Fireglow)).

key: items/seeds.gardenseed_fireglow.displayname -3116902073936822956 v1 Revised
Fireglow Seed

key: items/seeds.gardenseed_leek.description -5471822404930108221 v1 Revised
When planted in the ((Garden)), these seeds will turn into a ((Leek)).

key: items/seeds.gardenseed_leek.displayname 5071213952062876466 v1 Revised
Leek Seed

key: items/seeds.gardenseed_lettuce.description -3461738109966381461 v1 Revised
When planted in the ((Garden)), these seeds will turn into ((Lettuce)).

key: items/seeds.gardenseed_lettuce.displayname -1363117049406305906 v1 Revised
Lettuce Seed

key: items/seeds.gardenseed_mushroomseed.description 4109675555863093065 v1 Revised
This mysterious seed is moving by itself. How odd! It must be important. I should plant it in the ((Garden)) quickly.

key: items/seeds.gardenseed_mushroomseed.displayname -7525295701106903616 v1 Revised
Mysterious Seed

key: items/seeds.gardenseed_mystery.description -5272092200911534667 v1 Revised
What's this? When planted in the ((Garden)), it could turn into anything. Hopefully, something good.

key: items/seeds.gardenseed_mystery.displayname 1413028560370235396 v1 Revised
Odd Seed

key: items/seeds.gardenseed_onion.description -1244819376036863899 v1 Revised
When planted in the ((Garden)), these seeds will turn into an ((Onion)).

key: items/seeds.gardenseed_onion.displayname -3099632898397843570 v1 Revised
Onion Seed

key: items/seeds.gardenseed_potato.description -4889370586567428055 v1 Revised
When planted in the ((Garden)), these seeds will turn into a delicious ((Potato)).

key: items/seeds.gardenseed_potato.displayname -1632522094484309969 v1 Revised
Potato Seed

key: items/seeds.gardenseed_turnip.description -1382131417105589319 v1 Revised
When planted in the ((Garden)), these seeds will turn into a delectable ((Turnip)).

key: items/seeds.gardenseed_turnip.displayname 3740159304231147671 v1 Revised
Turnip Seed

key: items/seeds.orchardseed_apple.description -496238439292107776 v1 Revised
When planted in the ((Orchard)), this seed will grow into a beautiful ((Apple)) tree.

key: items/seeds.orchardseed_apple.displayname -2098978407458076741 v1 Revised
Apple Seed

key: items/seeds.orchardseed_butterfly.description 2121201814834577433 v1 Placeholder
When planted in the ((orchard)), this seed will grow into something strange.

key: items/seeds.orchardseed_butterfly.displayname 13341017661514050 v1 Placeholder
Strange Seed

key: items/seeds.orchardseed_cherry.description 3359582686842898912 v1 Revised
When planted in the ((Orchard)), this seed will grow into a gorgeous ((Cherry)) tree.

key: items/seeds.orchardseed_cherry.displayname -3948382632302505934 v1 Revised
Cherry Seed

key: items/seeds.orchardseed_mulberry.description 4404951207180362147 v1 Revised
When planted in the ((Orchard)), this seed will grow into a ((Mulberry)) tree, where nice silk worms will live. ((Silk Fibres)) will then grow, and nothing else.

key: items/seeds.orchardseed_mulberry.displayname 7258433146344332469 v1 Revised
Mulberry Seed

key: items/seeds.orchardseed_olive.description 4667609412530883321 v1 Revised
When planted in the ((Orchard)), this seed will grow into a venerable ((Olive)) tree.

key: items/seeds.orchardseed_olive.displayname 7834380588129881579 v1 Revised
Olive Seed

key: items/seeds.orchardseed_peach.description -25589811081962471 v1 Revised
When planted in the ((Orchard)), this seed will grow into a sumptuous and tall ((Peach)) tree.

key: items/seeds.orchardseed_peach.displayname -2317720301149550597 v1 Revised
Peach Seed

key: items/seeds.orchardseed_pear.description -8031551724142024173 v1 Revised
When planted in the ((Orchard)), this seed will grow into an opulent ((Pear)) tree.

key: items/seeds.orchardseed_pear.displayname 8044819169652398009 v1 Revised
Pear Seed

