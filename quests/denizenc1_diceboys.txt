key: quests/denizenc1_diceboys.diceboys._flavortextloc 8884848581923365296 v1 Revised
This hip-hop act needs a little exposure boost. I'll be their one-person street team!

key: quests/denizenc1_diceboys.diceboys._namekeyloc -629370428240022139 v1 Revised
Dice Boys presents...

key: quests/denizenc1_diceboys.talktodiceleader._descriptionloc 7271665958729533169 v1 Revised
Talk to the Dice Boys.

key: quests/denizenc1_diceboys.vinylone._descriptionloc -4122616912505135132 v1 Revised
Talk to someone in ((Hummingberg)).

key: quests/denizenc1_diceboys.vinylthree._descriptionloc 2435364359627162923 v1 Revised
Talk to someone in ((Bottom Line Corp.))

key: quests/denizenc1_diceboys.vinyltwo._descriptionloc 7857568240646508494 v1 Revised
Talk to someone in ((Furogawa)).

